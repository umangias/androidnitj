package com.example.android.nitj;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.android.nitj.Academics.AcademicsActivity;
import com.example.android.nitj.Login.UserDetaildActivity;
import com.firebase.ui.auth.data.model.User;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileFragment extends Fragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private DocumentReference userDoc = db.collection("Nit").document("User");
    private UserTaskAdapter adapter;
    private RecyclerView userTaskRv;
    private RelativeLayout editProfileRL;

    private String currentUserID;
    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;
    private StorageReference userProfileImageRef;

    private TextView userName,userYear,userBranch;
    private CircleImageView userProfilePic;

    private View v;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_user_profile, container, false);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();
        userProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Image");

        initializeFields();

        setUpRecyclerView();

        RetrieveUserInfo();

        editProfileRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserDetaildActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    private void setUpRecyclerView() {
        Query query = db.collection("Nit").document("User").collection("Usertasks");
        FirestoreRecyclerOptions<UserTask> options = new FirestoreRecyclerOptions.Builder<UserTask>()
                .setQuery(query,UserTask.class)
                .build();
        adapter = new UserTaskAdapter(options);
        RecyclerView recyclerView = v.findViewById(R.id.user_profile_task_rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    private void RetrieveUserInfo() {
        rootRef.child("Users").child(currentUserID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            if (dataSnapshot.hasChild("image")){
                                String retrieveProfleImage = dataSnapshot.child("image").getValue().toString();

                                Picasso.get().load(retrieveProfleImage)
                                        .into(userProfilePic);
                            }
                            if (dataSnapshot.hasChild("name")){
                                String retrieveName = dataSnapshot.child("name").getValue().toString();
                                userName.setText(retrieveName);

                            }
                            if (dataSnapshot.hasChild("branch")){
                                String retrieveBranch = dataSnapshot.child("branch").getValue().toString();
                                userBranch.setText(retrieveBranch);
                            }
                            if (dataSnapshot.hasChild("year")){
                                String retrieveYear = dataSnapshot.child("year").getValue().toString();
                                userYear.setText(retrieveYear);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }

    public void intentWebView(String webUrl) {
        Intent intent = new Intent(getContext(), WebViewClass.class);
        intent.putExtra("webUrl", webUrl);
        Toast.makeText(getActivity(), "Please make sure you are conected to NITJ network.", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }

    private void initializeFields() {
        editProfileRL = (RelativeLayout) v.findViewById(R.id.user_edit_profile_rl);
        userProfilePic = (CircleImageView) v.findViewById(R.id.user_profile_pic);
        userName = (TextView) v.findViewById(R.id.userName);
        userBranch = (TextView) v.findViewById(R.id.user_branch_name);
        userYear = (TextView) v.findViewById(R.id.user_study_year);
    }


    public class UserTaskAdapter extends FirestoreRecyclerAdapter<UserTask, UserTaskAdapter.TaskHolder> {

        public UserTaskAdapter(@NonNull FirestoreRecyclerOptions<UserTask> options) {
            super(options);
        }

        @Override
        protected void onBindViewHolder(@NonNull TaskHolder holder, int position, @NonNull final UserTask model) {
            holder.taskName.setText(model.getTaskName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentWebView(model.getTaskUrl());
                }
            });
        }

        @Override
        public int getItemCount() {
            return super.getItemCount();
        }

        @NonNull
        @Override
        public TaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_profile_tasks_list,
                    parent, false);
            return new TaskHolder(v);
        }


        class TaskHolder extends RecyclerView.ViewHolder {
            TextView taskName;


            public TaskHolder(@NonNull View itemView) {
                super(itemView);
                taskName = itemView.findViewById(R.id.user_profile_task_name);
            }
        }
    }
    }

