package com.example.android.nitj.Academics;

public class Branch {

    private String mBranch;
    private int mBranchImage;

    public Branch(String branch){
        this.mBranch = branch;
//        this.mBranchImage = branchImage;
    }

    public String getmBranch() {
        return mBranch;
    }

    public void setmBranch(String mBranch) {
        this.mBranch = mBranch;
    }

    public int getmBranchImage() {
        return mBranchImage;
    }

    public void setmBranchImage(int mBranchImage) {
        this.mBranchImage = mBranchImage;
    }
}
