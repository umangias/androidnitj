package com.example.android.nitj.Home.Buttons.Blog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;


public class BlogAdapter extends FirestoreRecyclerAdapter<Blog,BlogAdapter.BlogHolder> {
    private Context mContext;
    public BlogAdapter(@NonNull FirestoreRecyclerOptions<Blog> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull BlogHolder holder, int position, @NonNull Blog model) {
        holder.titleTextView.setText(model.getmTitle());
        holder.authorTextView.setText(model.getmAuthor());
        holder.subTitleTextView.setText(model.getmInfo());
        Glide.with(holder.photoImageView.getContext())
                .load(model.getmPhotoUrl())
                .fitCenter()
                .into(holder.photoImageView);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public BlogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_list,
                parent,false);
        return new BlogHolder(v);
    }

    class BlogHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView authorTextView;
        ImageView photoImageView;
        TextView subTitleTextView;
        public BlogHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title);
            authorTextView = itemView.findViewById(R.id.author);
            photoImageView = (ImageView) itemView.findViewById(R.id.blogsImage);
            subTitleTextView = (TextView) itemView.findViewById(R.id.subTitle);
        }
    }
}
