package com.example.android.nitj.Home.Buttons.Clubs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.android.nitj.Academics.FacultyAdapter;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ClubMemberActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ClubMemberAdapter adapterClub;
    private FacultyAdapter adapterFaculty;
    private RecyclerView memberRv;
    private static String clubName;
    private static String memberType;

    // here i m using same activity and layout of club members for faculty so modifying club member for faculty by using different adapter and taking intent
    // for type of member whether faculty or club.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_member);

        Intent intent = getIntent();
        clubName = intent.getStringExtra("club_name");
        memberType = intent.getStringExtra("member_type");

        if(memberType.equals("clubs")){
            setUpRecyclerAdapterClub();
        }

    }

    private void setUpRecyclerAdapterClub() {
        Query query = db.collection("Nit").document("Home").collection("Clubs")
                .document("Club_Members").collection(clubName);
        FirestoreRecyclerOptions<ClubMember> options = new FirestoreRecyclerOptions.Builder<ClubMember>()
                .setQuery(query,ClubMember.class)
                .build();
        adapterClub = new ClubMemberAdapter(options);
        memberRv = (RecyclerView) findViewById(R.id.club_member_recycler);
        memberRv.setLayoutManager(new GridLayoutManager(this,2));
        memberRv.setHasFixedSize(true);
        adapterClub.notifyDataSetChanged();
        memberRv.setAdapter(adapterClub);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(memberType.equals("clubs")){
            adapterClub.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(memberType.equals("clubs")){
            adapterClub.stopListening();
        }
    }
}
