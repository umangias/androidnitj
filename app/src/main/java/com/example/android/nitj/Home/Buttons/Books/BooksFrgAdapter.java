package com.example.android.nitj.Home.Buttons.Books;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class BooksFrgAdapter extends FragmentPagerAdapter {
    private Context mContext;

    public BooksFrgAdapter(Context context,FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        if(i==0)
            return new BtechBooksFragment();
        else if(i==1)
            return new MtechBooksFragment();
        else
            return new PhdBooksFragment();

   }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "B.Tech";
        }
        else if(position ==1){
            return "M.Tech";
        }
        else
            return "PhD";

    }

    @Override
    public int getCount() {
        return 3;
    }
}
