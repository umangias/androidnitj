package com.example.android.nitj.Home.Buttons.Updates;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;

public class UpdateAdapter extends FirestoreRecyclerAdapter<Update,UpdateAdapter.UpdateHolder> {
    Context context;
    public UpdateAdapter(@NonNull FirestoreRecyclerOptions<Update> options,Context context) {
        super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull UpdateHolder holder, int position, @NonNull final Update model) {
        holder.updateName.setText(model.getUpdateName());
        holder.updateName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(model.getUpdateUrl()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public UpdateHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.update_list_layout,viewGroup,false);

        return new UpdateHolder(v);
    }

    public class UpdateHolder extends RecyclerView.ViewHolder {
        TextView updateName;
        public UpdateHolder(@NonNull View itemView) {
            super(itemView);
            updateName = itemView.findViewById(R.id.updateName);
        }
    }
}
