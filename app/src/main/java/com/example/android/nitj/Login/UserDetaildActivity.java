package com.example.android.nitj.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.android.nitj.MainActivity;
import com.example.android.nitj.R;
import com.example.android.nitj.UserProfileFragment;
import com.firebase.ui.auth.data.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import io.grpc.Context;

public class UserDetaildActivity extends AppCompatActivity {
    private Button updateProfile;
    private EditText userName,userBranch,userYear;
    private CircleImageView userProfileImage;
    private Spinner courseSpinner;
    private String currentUserID;
    private FirebaseAuth mAuth;
    private DatabaseReference rootRef;
    private StorageReference userProfileImageRef;
    private ProgressDialog loadingBar;
    private Toolbar profileToolbar;

    private static String FLAG = "B.Tech";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detaild);
        
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        rootRef = FirebaseDatabase.getInstance().getReference();
        userProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Image");
        
        initializeFields();

        setupSpinner();

        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileInfo();
            }
        });

        RetrieveUserInfo();

        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .start(UserDetaildActivity.this);
            }
        });

    }

    private void RetrieveUserInfo() {
        rootRef.child("Users").child(currentUserID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            if (dataSnapshot.hasChild("image")){
                                String retrieveProfleImage = dataSnapshot.child("image").getValue().toString();

                                Picasso.get().load(retrieveProfleImage)
                                        .into(userProfileImage);
                            }
                            if (dataSnapshot.hasChild("name")){
                                String retrieveName = dataSnapshot.child("name").getValue().toString();
                                userName.setText(retrieveName);

                            }
                            if (dataSnapshot.hasChild("branch")){
                                String retrieveBranch = dataSnapshot.child("branch").getValue().toString();
                                userBranch.setText(retrieveBranch);
                            }
                            if (dataSnapshot.hasChild("year")){
                                String retrieveYear = dataSnapshot.child("year").getValue().toString();
                                userYear.setText(retrieveYear);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }

    private void updateProfileInfo() {
        String setUserName = userName.getText().toString();
        String setUserBranch = userBranch.getText().toString();
        String setUserYear = userYear.getText().toString();


        if (TextUtils.isEmpty(setUserName)) {
            Toast.makeText(this, "Please enter your Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(setUserBranch)) {
            Toast.makeText(this, "Please enter your Branch", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(setUserYear)) {
            Toast.makeText(this, "Please enter your year", Toast.LENGTH_SHORT).show();
        }else {
            HashMap<String, Object> profileMap = new HashMap<>();
            profileMap.put("uid", currentUserID);
            profileMap.put("name", setUserName);
            profileMap.put("branch", setUserBranch);
            profileMap.put("year", setUserYear);
            profileMap.put("course",FLAG);

            rootRef.child("Users").child(currentUserID).updateChildren(profileMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(UserDetaildActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                                SendUserToMainActivity();
                            }
                            else {
                                String message = task.getException().toString();
                                Toast.makeText(UserDetaildActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK){
                loadingBar.setTitle("Set Profile Image");
                loadingBar.setMessage("Please wait your profile image is uploading");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();

                Uri resultUri = result.getUri();
                final StorageReference filePath = userProfileImageRef.child(currentUserID + ".jpg");
                filePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()){
                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    rootRef.child("Users").child(currentUserID).child("image")
                                            .setValue(uri.toString())
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        loadingBar.dismiss();
                                                        Toast.makeText(UserDetaildActivity.this, "Image saved Successfully to database", Toast.LENGTH_SHORT).show();
                                                    }
                                                    else{
                                                        String message = task.getException().toString();
                                                        Toast.makeText(UserDetaildActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
                                                        loadingBar.dismiss();
                                                    }
                                                }
                                            });
                                }
                            });
                        }
                        else{
                            String message = task.getException().toString();
                            Toast.makeText(UserDetaildActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
                            loadingBar.dismiss();
                        }
                    }
                });

            }
        }
    }

    private void initializeFields() {
        updateProfile = (Button) findViewById(R.id.bt_user_update_profile);
        userName = (EditText) findViewById(R.id.set_userName);
        userBranch = (EditText) findViewById(R.id.set_userBranch);
        userYear = (EditText) findViewById(R.id.set_userYear);
        userProfileImage = (CircleImageView) findViewById(R.id.set_user_profile_image);
        courseSpinner = (Spinner) findViewById(R.id.set_user_course);
        loadingBar = new ProgressDialog(this);

        profileToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(profileToolbar);
    }

    private void setupSpinner() {

        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        courseSpinner.setAdapter(genderSpinnerAdapter);
        courseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.btech))) {
                        FLAG = "B.Tech";

                    } else if (selection.equals(getString(R.string.mtech))) {
                        FLAG = "M.Tech";
                    }
                    else if (selection.equals(getString(R.string.phd))) {
                        FLAG = "PhD";
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void SendUserToMainActivity() {
        Intent mainIntent = new Intent(UserDetaildActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

}
