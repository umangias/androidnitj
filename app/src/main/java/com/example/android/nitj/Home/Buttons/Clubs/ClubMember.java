package com.example.android.nitj.Home.Buttons.Clubs;

public class ClubMember {
    private String name;
    private String branch;
    private String imageUrl;
    private String facultyName;

    private ClubMember(){}

    public ClubMember(String name,String branch,String image,String facultyName){
        this.name = name;
        this.branch = branch;
        this.imageUrl = image;
        this.facultyName = facultyName;
    }


    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
