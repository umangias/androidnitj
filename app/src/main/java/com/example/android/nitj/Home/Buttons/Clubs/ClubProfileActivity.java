package com.example.android.nitj.Home.Buttons.Clubs;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.nitj.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

public class ClubProfileActivity extends  YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private CardView clubMembers;
    String clubName;
    TextView clubProfileName;
    private DocumentReference myRef;
    private DocumentReference mRef;

    private static final String API_KEY = "AIzaSyC72invOqeTq3HMfz05a4FyJ9EOWYftPNo";
    private static String VIDEO_ID = "tmeE1YIgjHE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_profile);

        clubProfileName = findViewById(R.id.club_profile_name);


        YouTubePlayerView youTube = (YouTubePlayerView) findViewById(R.id.youtube_player);
        youTube.initialize(API_KEY,this);

        Intent intent = getIntent();
        clubName = intent.getStringExtra("club_name");


        myRef = FirebaseFirestore.getInstance().collection("Nit").document("Home").collection("Clubs").document("Club_Profile").collection(clubName).document("detail");
        myRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable final DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                clubProfileName.setText((String)documentSnapshot.get("clubName"));
            }

        });

        clubMembers = (CardView) findViewById(R.id.club_members);
        clubMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClubProfileActivity.this,ClubMemberActivity.class);
                intent.putExtra("club_name",clubName);
                intent.putExtra("member_type","clubs");
                startActivity(intent);
            }
        });
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, final boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);


        if(!wasRestored) {
            mRef = FirebaseFirestore.getInstance().collection("Nit").document("Home").collection("Clubs").document("Club_Profile").collection("Criminals").document("detail");
            mRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {

                    youTubePlayer.cueVideo((String) documentSnapshot.get("videoId"));

                }
            });
        }

    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }

        @Override
        public void onVideoStarted() {
        }
    };

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();

    }

//    public static String getVideoId() {
//        return VIDEO_ID;
//    }
//
//    public static void setVideoId(String videoId) {
//        VIDEO_ID = videoId;
//    }
}
