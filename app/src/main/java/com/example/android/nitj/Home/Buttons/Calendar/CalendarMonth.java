package com.example.android.nitj.Home.Buttons.Calendar;

public class CalendarMonth {
    private String month;
    private String imageUrl;

    private CalendarMonth(){}

    public CalendarMonth(String month, String imageUrl){
        this.month = month;
        this.imageUrl = imageUrl;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
