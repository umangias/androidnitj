package com.example.android.nitj.Home.Buttons.Photo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class PhotoDataAdapter extends FirestoreRecyclerAdapter<Photo,PhotoDataAdapter.PhotoHolder> {

    public PhotoDataAdapter(@NonNull FirestoreRecyclerOptions<Photo> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull PhotoHolder holder, int position, @NonNull Photo model) {
        holder.photoClickerName.setText(model.getmName());
        holder.caption.setText(model.getmCaption());
        Glide.with(holder.photo.getContext())
                .load(model.getmPhotoUrl())
                .fitCenter()
                .into(holder.photo);

    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public PhotoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.photo_list,
                viewGroup,false);
        return new PhotoHolder(v);
    }

    public class PhotoHolder extends RecyclerView.ViewHolder {
        TextView photoClickerName;
        ImageView photo;
        TextView caption;
        public PhotoHolder(@NonNull View itemView) {
            super(itemView);
            photoClickerName = itemView.findViewById(R.id.photoClicker);
            photo = itemView.findViewById(R.id.photo);
            caption = itemView.findViewById(R.id.captionPhoto);
        }

    }
}
