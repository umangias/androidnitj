package com.example.android.nitj.Home.Buttons.Photo;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PhotoAdapter extends FragmentPagerAdapter {
    private Context mContext;
    public PhotoAdapter(Context context,FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        if(i==0){
            return new PhotoFragment();
        }
        else if(i==1)
            return new GalleryFragment();
        else
            return new VideoFragment();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "Photos";
        }
        else if(position ==1){
            return "Gallery";
        }
        else
            return "Videos";

    }

    @Override
    public int getCount() {
        return 3;
    }
}
