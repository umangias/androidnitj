package com.example.android.nitj.Home.Buttons.Events;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class EventAdapter extends FirestoreRecyclerAdapter<Event,EventAdapter.EventHolder> {

    public EventAdapter(@NonNull FirestoreRecyclerOptions<Event> options) {
        super(options);
    }
    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull final EventHolder holder, int position, @NonNull final Event model) {
        holder.dayTv.setText(model.getEventDay());
        holder.monthTv.setText(model.getEventMonth());
        holder.locatioTv.setText(model.getEventLocation());
        holder.nameTv.setText(model.getEventName());
        Glide.with(holder.eventPhoto.getContext())
                .load(model.getEventPhotoUrl())
                .fitCenter()
                .into(holder.eventPhoto);

        final Context context = holder.featuredCard.getContext();

        holder.featuredCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.featuredCard.getContext(),EventsDetailActivity.class);
                intent.putExtra("event_id",model.getEventId());
                context.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.featured_events_list,viewGroup,false);

        return new EventHolder(v);
    }

    public class EventHolder extends RecyclerView.ViewHolder {
        TextView dayTv;
        TextView monthTv;
        TextView locatioTv;
        TextView nameTv;
        ImageView eventPhoto;
        CardView featuredCard;
        public EventHolder(@NonNull final View itemView) {
            super(itemView);
            dayTv = itemView.findViewById(R.id.event_day);
            monthTv = itemView.findViewById(R.id.event_month);
            locatioTv = itemView.findViewById(R.id.event_location);
            nameTv = itemView.findViewById(R.id.event_name);
            eventPhoto = itemView.findViewById(R.id.event_photo);
            featuredCard = itemView.findViewById(R.id.featured_event_card);

        }
    }
}
