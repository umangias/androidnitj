package com.example.android.nitj.Home.Buttons.Photo;

public class Photo {
    private String mName;
    private String mPhotoUrl;
    private String mCaption;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPhotoUrl() {
        return mPhotoUrl;
    }

    public void setmPhotoUrl(String mPhotoUrl) {
        this.mPhotoUrl = mPhotoUrl;
    }

    private Photo(){

    }

    public String getmCaption() {
        return mCaption;
    }

    public void setmCaption(String mCaption) {
        this.mCaption = mCaption;
    }

    public Photo(String name, String caption, String photoUrl){
        this.mName = name;
        this.mCaption = caption;
        this.mPhotoUrl = photoUrl;
    }


}

