package com.example.android.nitj.Home.Buttons.Sports;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;

public class SportsActivity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private SportsAdapter adapter;
    private RecyclerView matchRv;
    private TournamentAdapter adapterTour;
    private RecyclerView tourRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUpRecyclerAdapterForMactch();
        setUpRecyclerAdapterForTournament();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpRecyclerAdapterForTournament() {
        Query query = db.collection("Nit").document("Home").collection("Sports")
                .document("Tornament").collection("Events");
        FirestoreRecyclerOptions<Tournament> options = new FirestoreRecyclerOptions.Builder<Tournament>()
                .setQuery(query,Tournament.class)
                .build();
        adapterTour = new TournamentAdapter(options);
        tourRv = findViewById(R.id.recycler_list_sports);
        tourRv.setHasFixedSize(true);
        tourRv.setLayoutManager(new LinearLayoutManager(this));
        adapterTour.notifyDataSetChanged();
        tourRv.setAdapter(adapterTour);
    }

    private void setUpRecyclerAdapterForMactch() {
        Query query = db.collection("Nit").document("Home").collection("Sports")
                .document("Matches").collection("Match");
        FirestoreRecyclerOptions<Sports> options = new FirestoreRecyclerOptions.Builder<Sports>()
                .setQuery(query,Sports.class)
                .build();
        adapter = new SportsAdapter(options);
        matchRv = findViewById(R.id.recycler_list_matches);
        matchRv.setHasFixedSize(true);
        matchRv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        adapter.notifyDataSetChanged();
        matchRv.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
        adapterTour.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
        adapterTour.stopListening();
    }

}
