package com.example.android.nitj.Home.Buttons.Books;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.nitj.R;


public class BtechBooksFragment extends Fragment {

    View v;
    private TextView sem1;
    private TextView sem2;
    private TextView sem3;
    private TextView sem4;
    private TextView sem5;
    private TextView sem6;
    private TextView sem7;
    private TextView sem8;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_btech_books, container, false);

        sem1 = (TextView) v.findViewById(R.id.Sem_1);
        sem2 = (TextView) v.findViewById(R.id.Sem_2);
        sem3 = (TextView) v.findViewById(R.id.Sem_3);
        sem4 = (TextView) v.findViewById(R.id.Sem_4);
        sem5 = (TextView) v.findViewById(R.id.Sem_5);
        sem6 = (TextView) v.findViewById(R.id.Sem_6);
        sem7 = (TextView) v.findViewById(R.id.Sem_7);
        sem8 = (TextView) v.findViewById(R.id.Sem_8);  //textView declaration IDs

        sem1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem1");
                startActivity(intent);
            }
        });
        sem2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem2");
                startActivity(intent);
            }
        });
        sem3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem3");
                startActivity(intent);
            }
        });
        sem4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem4");
                startActivity(intent);
            }
        });
        sem5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem5");
                startActivity(intent);
            }
        });
        sem6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem6");
                startActivity(intent);
            }
        });
        sem7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem7");
                startActivity(intent);
            }
        });
        sem8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),BooksPdfActivity.class);
                intent.putExtra("semester_no","sem8");
                startActivity(intent);
            }
        });



        return v;
    }


}
