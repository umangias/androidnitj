package com.example.android.nitj.Home.Buttons.Clubs;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ClubsFragmentAdapter extends FragmentPagerAdapter {
    private Context mContext;
    public ClubsFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        if(i==0){
            return new ClubsFragment();
        }
        else
            return new SocietyFragment();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "Clubs";
        }
        else
            return "Society";

    }

    @Override
    public int getCount() {
        return 2;
    }
}
