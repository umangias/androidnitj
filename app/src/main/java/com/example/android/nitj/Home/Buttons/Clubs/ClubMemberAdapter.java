package com.example.android.nitj.Home.Buttons.Clubs;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import de.hdodenhof.circleimageview.CircleImageView;

public class ClubMemberAdapter extends FirestoreRecyclerAdapter<ClubMember,ClubMemberAdapter.ClubMemberHolder> {

    public ClubMemberAdapter(@NonNull FirestoreRecyclerOptions<ClubMember> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ClubMemberHolder holder, int position, @NonNull ClubMember model) {
        holder.name.setText(model.getName());
        holder.branch.setText(model.getBranch());
        Glide.with(holder.image.getContext())
                .load(model.getImageUrl())
                .fitCenter()
                .into(holder.image);
    }

    @NonNull
    @Override
    public ClubMemberHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.club_members_list,viewGroup,false);
        return new ClubMemberHolder(v);
    }

    public class ClubMemberHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView branch;
        CircleImageView image;
        public ClubMemberHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.club_member_name);
            branch = (TextView) itemView.findViewById(R.id.club_member_branch);
            image = (CircleImageView) itemView.findViewById(R.id.club_member_image);
        }
    }
}
