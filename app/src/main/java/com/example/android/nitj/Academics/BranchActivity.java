package com.example.android.nitj.Academics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.nitj.Home.Buttons.Books.BooksActivity;
import com.example.android.nitj.Home.Buttons.Clubs.ClubMember;
import com.example.android.nitj.Home.Buttons.Clubs.ClubMemberActivity;
import com.example.android.nitj.Home.Buttons.Photo.Gallery;
import com.example.android.nitj.Home.Buttons.Photo.GalleryActivity;
import com.example.android.nitj.Nit;
import com.example.android.nitj.NitAdapter;
import com.example.android.nitj.R;
import com.example.android.nitj.SimpleGestureFilter;
import com.example.android.nitj.WebViewClass;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class BranchActivity extends AppCompatActivity implements SimpleGestureFilter.SimpleGestureListener {
    SimpleGestureFilter detector;
    private CardView faculties;
    private ImageView branchImage;
    private Toolbar toolbar;
    private static String branch;
    private static String webUrl;

    private NitAdapter adapter;
    private FacultyAdapter adapterFaculty;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch);
        toolbar = findViewById(R.id.branch_toolbar);
        detector = new SimpleGestureFilter(this, this);

        Intent intent = getIntent();
        branch = intent.getStringExtra("branch");
        webUrl = intent.getStringExtra("webUrl");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(branch);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        faculties = (CardView) findViewById(R.id.branch_faculties);
        faculties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BranchActivity.this, ClubMemberActivity.class);
                intent.putExtra("branch", branch);
                intent.putExtra("member_type", "faculty");
                startActivity(intent);
            }
        });

        setUpRecyclerAdapterCards();
        setUpRecyclerAdapterFaculty();
    }

    private void setUpRecyclerAdapterCards() {
//        Query query = db.collection("Nit").document("Academics").collection(branch).document("branchCards").collection("Cards");
        // data has to be added so using data of home cards
        Query query = db.collection("Nit").document("Home").collection("homeCards");

        FirestoreRecyclerOptions<Nit> options = new FirestoreRecyclerOptions.Builder<Nit>()
                .setQuery(query, Nit.class)
                .build();
        adapter = new NitAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.branch_recycler_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private void setUpRecyclerAdapterFaculty() {
        Query query = db.collection("Nit").document("Academics").collection("Branch").document(branch).collection("Faculty");
        FirestoreRecyclerOptions<Member> options = new FirestoreRecyclerOptions.Builder<Member>()
                .setQuery(query,Member.class)
                .build();
        adapterFaculty = new FacultyAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.faculty_recycler_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        adapterFaculty.notifyDataSetChanged();
        recyclerView.setAdapter(adapterFaculty);
    }


    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
        adapterFaculty.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
        adapterFaculty.stopListening();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        this.detector.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onSwipe(int direction) {
        if (webUrl != null) {

            switch (direction) {

                case SimpleGestureFilter.SWIPE_RIGHT:
                    Intent intent = new Intent(this, GalleryActivity.class);
                    startActivity(intent);
                    break;

                case SimpleGestureFilter.SWIPE_LEFT:
                    intentWebView(webUrl);
                    Toast.makeText(this, "Opening, Official website for " + branch + " branch.", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            switch (direction) {

                case SimpleGestureFilter.SWIPE_RIGHT:
                    Intent intent = new Intent(this, GalleryActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onDoubleTap() {
//        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }

    public void intentWebView(String webUrl) {
        Intent intent = new Intent(BranchActivity.this, WebViewClass.class);
        intent.putExtra("webUrl", webUrl);
        startActivity(intent);
    }

}
