package com.example.android.nitj.Home.Buttons.Events;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class EventAdapter2 extends FirestoreRecyclerAdapter<Event,EventAdapter2.EventHolder2> {

    public EventAdapter2(@NonNull FirestoreRecyclerOptions<Event> options) {
        super(options);
    }
    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull EventHolder2 holder, int position, @NonNull final Event model) {
        holder.upDayTv.setText(model.getEventDay());
        holder.upMonthTv.setText(model.getEventMonth());
        holder.upLocatioTv.setText(model.getEventLocation());
        holder.upNameTv.setText(model.getEventName());
        Glide.with(holder.upEventPhoto.getContext())
                .load(model.getEventPhotoUrl())
                .fitCenter()
                .into(holder.upEventPhoto);

        final Context context = holder.upCard.getContext();
        holder.upCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,EventsDetailActivity.class);
                intent.putExtra("event_id",model.getEventId());
                context.startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public EventHolder2 onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.upcoming_event_list,viewGroup,false);

        return new EventHolder2(v);
    }

    public class EventHolder2 extends RecyclerView.ViewHolder {
        TextView upDayTv;
        TextView upMonthTv;
        TextView upLocatioTv;
        TextView upNameTv;
        ImageView upEventPhoto;
        CardView upCard;
        public EventHolder2(@NonNull final View itemView) {
            super(itemView);
            upDayTv = itemView.findViewById(R.id.up_event_day);
            upMonthTv = itemView.findViewById(R.id.up_event_month);
            upLocatioTv = itemView.findViewById(R.id.up_event_location);
            upNameTv = itemView.findViewById(R.id.up_event_name);
            upEventPhoto = itemView.findViewById(R.id.up_event_photo);
            upCard = itemView.findViewById(R.id.upcoming_event_card);
        }
    }
}
