package com.example.android.nitj;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;


public class NitAdapter extends FirestoreRecyclerAdapter<Nit,NitAdapter.NitHolder> {
    private Context mContext;
    public NitAdapter(@NonNull FirestoreRecyclerOptions<Nit> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull NitHolder holder, int position, @NonNull Nit model) {
        holder.descriptionCardHome.setText(model.getmCardDescription());
        Glide.with(holder.cardHome.getContext())
                .load(model.getmCardPhotoUrl())
                .fitCenter()
                .into(holder.cardHome);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public NitHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,
                parent,false);
        return new NitHolder(v);
    }

    class NitHolder extends RecyclerView.ViewHolder {
        TextView descriptionCardHome;
        ImageView cardHome;
        public NitHolder(@NonNull View itemView) {
            super(itemView);
            descriptionCardHome = itemView.findViewById(R.id.card_description);
            cardHome = itemView.findViewById(R.id.card_image);
        }
    }
}
