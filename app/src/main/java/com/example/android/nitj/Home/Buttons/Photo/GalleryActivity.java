package com.example.android.nitj.Home.Buttons.Photo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.android.nitj.R;

public class GalleryActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        toolbar = findViewById(R.id.gallery_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Gallery");
    }
}
