package com.example.android.nitj.Academics;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;

import java.util.ArrayList;

public class BranchAdapter extends ArrayAdapter {

    public BranchAdapter(Activity context, ArrayList<Branch> branches){
        super(context,0,branches);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.department_list_view,parent,false
            );

            Branch currentBranch = (Branch) getItem(position);
//            ImageView branchImage = listItemView.findViewById(R.id.branchDepartmentImage);
            TextView branchText = listItemView.findViewById(R.id.branchDepartmentText);

//            branchImage.setImageResource(currentBranch.getmBranchImage());
//            Glide.with(getContext())
//                    .load(currentBranch.getmBranchImage())
//                    .into(branchImage);
            branchText.setText(currentBranch.getmBranch());
        }
        return listItemView;
    }
}
