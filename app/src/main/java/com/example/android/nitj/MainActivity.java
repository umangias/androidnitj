package com.example.android.nitj;

import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.android.nitj.Home.Buttons.Books.BooksActivity;
import com.example.android.nitj.Home.Buttons.Events.EventsFragment;
import com.example.android.nitj.Login.LoginActivity;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;
import com.google.firebase.auth.FirebaseAuth.AuthStateListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static  final int RC_SIGN_IN = 1;
    private static final String ANONYMOUS = "anonymous";

    private BottomNavigationView bottomNavigation;
    private String mUsername;
    private FirebaseAuth mFirebaseAuth;
    private AuthStateListener mAuthStateListener;
    private DatabaseReference rootRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        rootRef = FirebaseDatabase.getInstance().getReference();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("NITJ");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        loadFragment(new HomeFragment());

        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment fragment =null;
                int id = menuItem.getItemId();
                switch(id){
                    case R.id.action_home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.action_events:
                        fragment = new EventsFragment();
                        break;
                    case R.id.action_user_profile:
                        fragment = new UserProfileFragment();
                        break;
                }
                return loadFragment(fragment);
            }
        });

//        mUsername = ANONYMOUS;
//
        mFirebaseAuth = FirebaseAuth.getInstance();
//        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if(user != null){
//                    onSignedInInitialize(user.getDisplayName());
//                }
//                else{
//                    onSignedOutCleanuo();
//                    List<AuthUI.IdpConfig> providers = Arrays.asList(
//                            new AuthUI.IdpConfig.GoogleBuilder().build(),
//                            new AuthUI.IdpConfig.FacebookBuilder().build(),
//                            new AuthUI.IdpConfig.EmailBuilder().build());
//                    startActivityForResult(
//                            AuthUI.getInstance()
//                                .createSignInIntentBuilder()
//                                .setAvailableProviders(providers)
//                                .build(),
//                            RC_SIGN_IN);
//
//                }
//            }
//        };
//        mFirebaseAuth.addAuthStateListener(mAuthStateListener);

// show case view code anywhere and 2 more lines above
//        // We load a drawable and create a location to show a tap target here
//        // We need the display to get the width and height at this point in time
//        final Display display = getWindowManager().getDefaultDisplay();
//        // Load our little droid guy
//        final Drawable droid = ContextCompat.getDrawable(this, R.drawable.target_view_black);
//        // Tell our droid buddy where we want him to appear
//        final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
//        // Using deprecated methods makes you look way cool
//        droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);
//
//        final SpannableString sassyDesc = new SpannableString("It allows you to go back, sometimes");
//        sassyDesc.setSpan(new StyleSpan(Typeface.ITALIC), sassyDesc.length() - "sometimes".length(), sassyDesc.length(), 0);
//
//        // We have a sequence of targets, so lets build it!
//        final TapTargetSequence sequence = new TapTargetSequence(this)
//                .targets(
//                        // This tap target will target the back button, we just need to pass its containing toolbar
//                        TapTarget.forToolbarNavigationIcon(toolbar, "This is the back button", sassyDesc).id(1),
//                        // Likewise, this tap target will target the search button
//                        TapTarget.forToolbarMenuItem(toolbar, R.id.action_search, "This is a search icon", "As you can see, it has gotten pretty dark around here...")
//                                .dimColor(android.R.color.black)
//                                .outerCircleColor(R.color.colorAccent)
//                                .targetCircleColor(android.R.color.black)
//                                .transparentTarget(true)
//                                .textColor(android.R.color.black)
//                                .id(2),
//                        // You can also target the overflow button in your toolbar
//                        TapTarget.forToolbarOverflow(toolbar, "This will show more options", "But they're not useful :(").id(3),
//                        // This tap target will target our droid buddy at the given target rect
//                        TapTarget.forBounds(droidTarget, "Oh look!", "You can point to any part of the screen. You also can't cancel this one!")
//                                .cancelable(false)
//                                .icon(droid)
//                                .id(4)
//                )
//                .listener(new TapTargetSequence.Listener() {
//                    // This listener will tell us when interesting(tm) events happen in regards
//                    // to the sequence
//                    @Override
//                    public void onSequenceFinish() {
////                        ((TextView) findViewById(R.id.educated)).setText("Congratulations! You're educated now!");
//                        Toast.makeText(MainActivity.this, "finished in sequence", Toast.LENGTH_SHORT).show();
//
//                    }
//
//                    @Override
//                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
//                        Log.d("TapTargetView", "Clicked on " + lastTarget.id());
//                    }
//
//                    @Override
//                    public void onSequenceCanceled(TapTarget lastTarget) {
//
//                    }
//                });
//
//        final SpannableString spannedDesc = new SpannableString("This is the sample app for TapTargetView");
//        spannedDesc.setSpan(new UnderlineSpan(), spannedDesc.length() - "TapTargetView".length(), spannedDesc.length(), 0);
//        TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.fab), "Hello, world!", spannedDesc)
//                .cancelable(false)
//                .drawShadow(true)
//                .titleTextDimen(R.dimen.title_text_size)
//                .tintTarget(false), new TapTargetView.Listener() {
//            @Override
//            public void onTargetClick(TapTargetView view) {
//                super.onTargetClick(view);
//                // .. which evidently starts the sequence we defined earlier
//                sequence.start();
//            }
//
//            @Override
//            public void onOuterCircleClick(TapTargetView view) {
//                super.onOuterCircleClick(view);
//                Toast.makeText(view.getContext(), "You clicked the outer circle!", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
//                Log.d("TapTargetViewSample", "You dismissed me :(");
//            }
//        });




    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(requestCode == RC_SIGN_IN){
//            if(resultCode == RESULT_OK){
//                Toast.makeText(this,"Signed In!",Toast.LENGTH_SHORT).show();;
//            }
//            else if(resultCode == RESULT_CANCELED){
//                Toast.makeText(this,"Sign in cancelled",Toast.LENGTH_SHORT).show();
//                finish();
//            }
//        }
//    }
//
//    private void onSignedOutCleanuo() {
//        mUsername = ANONYMOUS;
//    }
//
//    private void onSignedInInitialize(String displayName) {
//    }

    @Override
    protected void onResume() {
        super.onResume();
//        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if(mAuthStateListener != null){
//            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
//        }
    }

    private boolean loadFragment(Fragment fragment){
        if(fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container,fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.log_out){
            mFirebaseAuth.signOut();
            SendUserToLoginActivity();
        }

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser myCurrentUser = mFirebaseAuth.getCurrentUser();
        if (myCurrentUser == null){
            SendUserToLoginActivity();
        }

    }

    private void SendUserToLoginActivity() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
