package com.example.android.nitj.Home.Buttons.Calendar;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class CalendarActivity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CalEventsAdapter adapterEvents;
    private CalMonthAdapter adapterMonth;
    private RecyclerView monthRv;
    private RecyclerView eventsRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        setUpCalendarEventRV();
        setUpCalendarMonthRV();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void setUpCalendarMonthRV() {
        Query query = db.collection("Nit").document("Home").collection("Calendar");
        FirestoreRecyclerOptions<CalendarMonth> options = new FirestoreRecyclerOptions.Builder<CalendarMonth>()
                        .setQuery(query,CalendarMonth.class)
                        .build();
        adapterMonth = new CalMonthAdapter(options);
        monthRv = findViewById(R.id.calendar);
        monthRv.setHasFixedSize(true);
        monthRv.setLayoutManager(new LinearLayoutManager(this));
        adapterMonth.notifyDataSetChanged();
        monthRv.setAdapter(adapterMonth);

    }

//    private void setUpCalendarEventRV() {
//        Query query = db.collection("Nit").document("Home").collection("Calendar");
//        FirestoreRecyclerOptions<CalEvents> options = new FirestoreRecyclerOptions.Builder<CalEvents>()
//                .setQuery(query,CalEvents.class)
//                .build();
//        adapterEvents = new CalEventsAdapter(options);
//        eventsRv = findViewById(R.id.calendar_events);
//        eventsRv.setHasFixedSize(true);
//        eventsRv.setLayoutManager(new LinearLayoutManager(this));
//        adapterEvents.notifyDataSetChanged();
//        eventsRv.setAdapter(adapterEvents);
//
//    }

    @Override
    protected void onStart() {
        super.onStart();
        adapterMonth.startListening();
//        adapterEvents.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapterMonth.stopListening();
//        adapterEvents.stopListening();
    }
}
