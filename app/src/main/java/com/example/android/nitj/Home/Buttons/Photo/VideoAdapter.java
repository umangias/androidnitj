package com.example.android.nitj.Home.Buttons.Photo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

public class VideoAdapter extends FirestoreRecyclerAdapter<Video,VideoAdapter.VideoHolder> {

    public VideoAdapter(@NonNull FirestoreRecyclerOptions<Video> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull VideoHolder holder, int position, @NonNull Video model) {
//        holder.videoView.loadData(model.getVideoUrl(),"text/html" , "utf-8");
//        holder.videoView.loadUrl(model.getVideoUrl());
        holder.videoView.loadData( model.getVideoUrl(), "text/html" , "utf-8" );
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.phot_video_list,viewGroup,false);
        return new VideoHolder(v);
    }

    public class VideoHolder extends RecyclerView.ViewHolder implements YouTubePlayer.OnInitializedListener {
        WebView videoView;
        public VideoHolder(@NonNull View itemView) {
            super(itemView);

            videoView = (WebView) itemView.findViewById(R.id.video_list);
            videoView.getSettings().setJavaScriptEnabled(true);
//            videoView.setWebViewClient(new WebViewClient());
            videoView.setWebChromeClient(new WebChromeClient());
        }

        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

        }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        }
    }
}
