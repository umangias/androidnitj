package com.example.android.nitj.Home.Buttons.Events;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

public class EventsDetailActivity extends AppCompatActivity {

    private String eventId;
    private DocumentReference myRef;

    private ImageView eventImage;
    private TextView eventLocation;
    private TextView eventDate;
    private TextView eventTimings;
    private TextView eventContent1;
    private TextView eventContent2;
    private CardView register;
    private CardView contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_detail);
        setSupportActionBar((Toolbar)findViewById(R.id.event_detail_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        eventImage = findViewById(R.id.event_detail_image);
        eventLocation = findViewById(R.id.event_detail_location);
        eventDate = findViewById(R.id.event_detail_date);
        eventTimings = findViewById(R.id.event_detail_timings);
        eventContent1 = findViewById(R.id.event_detail_content1);
        eventContent2 = findViewById(R.id.event_detail_content2);
        register = (CardView) findViewById(R.id.event_detail_register);
        contact = (CardView) findViewById(R.id.event_detail_contact);

        Intent intent = getIntent();
        eventId = intent.getStringExtra("event_id");

        myRef = FirebaseFirestore.getInstance().collection("Nit").document("Event").collection("EventDetail").document(eventId);
        myRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable final DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_event);
                collapsingToolbar.setTitle((String)documentSnapshot.get("eventName"));
                eventLocation.setText((String)documentSnapshot.get("location"));
                eventDate.setText((String)documentSnapshot.get("date"));
                eventTimings.setText((String)documentSnapshot.get("timings"));
                eventContent1.setText((String)documentSnapshot.get("content1"));
                eventContent2.setText((String)documentSnapshot.get("content2"));
                Glide.with(getApplicationContext())
                        .load(documentSnapshot.get("imageUrl"))
                        .fitCenter()
                        .into(eventImage);

                contact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Uri u = Uri.parse("tel:" + (String)documentSnapshot.get("contact"));
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        startActivity(i);
                    }
                });

                register.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(documentSnapshot.getString("registerUrl")));
                        startActivity(intent);
                    }
                });

            }
        });
    }
}
