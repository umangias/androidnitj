package com.example.android.nitj.Home.Buttons.Books;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import org.w3c.dom.Text;

public class BooksAdapter extends FirestoreRecyclerAdapter<Books,BooksAdapter.BooksHolder> {

    public BooksAdapter(@NonNull FirestoreRecyclerOptions<Books> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull final BooksHolder holder, int position, @NonNull final Books model) {
        holder.name.setText(model.getName());
        holder.author.setText(model.getAuthor());
        Glide.with(holder.image.getContext())
                .load(model.getImage())
                .fitCenter()
                .into(holder.image);
        holder.booksCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = holder.booksCard.getContext();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(model.getBooksUrl()));
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public BooksHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.books_pdf_list,viewGroup,false);
        return new BooksHolder(v);
    }

    public class BooksHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView author;
        CardView booksCard;
        public BooksHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.bookName);
            image = (ImageView) itemView.findViewById(R.id.booksImage);
            author = (TextView) itemView.findViewById(R.id.bookAuthor);
            booksCard = (CardView) itemView.findViewById(R.id.booksPdfCard);

        }
    }
}
