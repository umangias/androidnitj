package com.example.android.nitj.Home.Buttons.Photo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class GalleryAdapter extends FirestoreRecyclerAdapter<Gallery,GalleryAdapter.GalleryHolder> {

    public GalleryAdapter(@NonNull FirestoreRecyclerOptions<Gallery> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull GalleryHolder holder, int position, @NonNull Gallery model) {
        Glide.with(holder.photo.getContext())
                .load(model.getmPhotoUrl())
                .fitCenter()
                .into(holder.photo);

    }
    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public GalleryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_photo_list,viewGroup,false);

        return new GalleryHolder(v);
    }

    public class GalleryHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        public GalleryHolder(@NonNull View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.galleryPhoto);
        }
    }
}
