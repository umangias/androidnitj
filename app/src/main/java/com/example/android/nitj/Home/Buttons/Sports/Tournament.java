package com.example.android.nitj.Home.Buttons.Sports;

public class Tournament {
    private String name;
    private String imageUrl;
    private String desc;

    private Tournament(){}

    public Tournament(String name, String imageUrl,String desc){
        this.name = name;
        this.imageUrl = imageUrl;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
