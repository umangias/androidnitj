package com.example.android.nitj.Home.Buttons.Calendar;

public class CalEvents {
    private String date;
    private String day;
    private String event;
    private String desc;

    private CalEvents(){}

    public CalEvents(String date,String day, String desc){
        this.date = date;
        this.desc = desc;
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
