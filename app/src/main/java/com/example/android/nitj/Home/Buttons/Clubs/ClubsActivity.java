package com.example.android.nitj.Home.Buttons.Clubs;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.android.nitj.Home.Buttons.Photo.PhotoAdapter;
import com.example.android.nitj.R;

public class ClubsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clubs);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ViewPager vp = (ViewPager) findViewById(R.id.clubsViewPager);
        ClubsFragmentAdapter adapter = new ClubsFragmentAdapter(this,getSupportFragmentManager());
        vp.setAdapter(adapter);

        TabLayout tab = (TabLayout) findViewById(R.id.clubsTab);
        tab.setupWithViewPager(vp);
    }

}
