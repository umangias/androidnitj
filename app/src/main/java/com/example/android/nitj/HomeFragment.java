package com.example.android.nitj;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.nitj.Academics.AcademicsActivity;
import com.example.android.nitj.Home.Buttons.Blog.BlogActivity;
import com.example.android.nitj.Home.Buttons.Books.BooksActivity;
import com.example.android.nitj.Home.Buttons.Calendar.CalendarActivity;
import com.example.android.nitj.Home.Buttons.ClassActivity;
import com.example.android.nitj.Home.Buttons.Clubs.ClubsActivity;
import com.example.android.nitj.Home.Buttons.Events.EventsFragment;
import com.example.android.nitj.Home.Buttons.Updates.UpdatesActivity;
import com.example.android.nitj.Home.News.NewsActivity;
import com.example.android.nitj.Home.Buttons.HostelActivity;
import com.example.android.nitj.Home.Buttons.LibraryActivity;
import com.example.android.nitj.Home.Buttons.MentorActivity;
import com.example.android.nitj.Home.Buttons.Photo.PhotoActivity;
import com.example.android.nitj.Home.Buttons.Sports.SportsActivity;
import com.example.android.nitj.Login.LoginActivity;
import com.example.android.nitj.Login.RegisterActivity;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;

public class HomeFragment extends Fragment {

    private static final String TAG = "MainActivity";
    private SimpleGestureFilter detector;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();


    private NitAdapter adapter;
    private View v;
    private TextView quoteTV;

    private RelativeLayout libraryCircleButton;
    private RelativeLayout mentorCircleButton;
    private RelativeLayout academicsCircleButton;
    private RelativeLayout chatCircleButton;

    private RelativeLayout downloadCircleButton;
    private RelativeLayout blogCircleButton;
    private RelativeLayout newsCircleButton;

    private RelativeLayout clubCircleButton;
    private RelativeLayout hostelCircleButton;
    private RelativeLayout sportsCircleButton;

    public HomeFragment() {
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home, container, false);
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);
        quoteTV = (TextView) v.findViewById(R.id.quote);
        final DocumentReference docRef = db.collection("Nit").document("Home").collection("Quotes").document("DailyQuotes");
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot snapshot,
                                FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    quoteTV.setText(snapshot.getString("quote"));
                } else {
                    Log.d(TAG, "Current data: null");
                }
            }
        });

        quoteTV.setSelected(true);

        intentForButtons();

        setUpRecyclerAdapter();


        return v;
    }




    private void intentForButtons() {

        downloadCircleButton = (RelativeLayout) v.findViewById(R.id.home_download_button);
        blogCircleButton = (RelativeLayout) v.findViewById(R.id.home_blog_button);
        newsCircleButton = (RelativeLayout) v.findViewById(R.id.home_news_button);
        sportsCircleButton = (RelativeLayout) v.findViewById(R.id.home_sports_button);

        libraryCircleButton = (RelativeLayout) v.findViewById(R.id.home_library_button);
        mentorCircleButton = (RelativeLayout) v.findViewById(R.id.home_mentor_button);
        academicsCircleButton = (RelativeLayout) v.findViewById(R.id.home_academics_button);
        chatCircleButton = (RelativeLayout) v.findViewById(R.id.home_chat_button);

        clubCircleButton = (RelativeLayout) v.findViewById(R.id.home_club_button);
        hostelCircleButton = (RelativeLayout) v.findViewById(R.id.home_hostel_button);
        academicsCircleButton = (RelativeLayout) v.findViewById(R.id.home_academics_button);

        academicsCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AcademicsActivity.class);
                startActivity(intent);
            }
        });


        hostelCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });

        clubCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivity(intent);
            }
        });



        downloadCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UpdatesActivity.class);
                startActivity(intent);
            }
        });
        blogCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BlogActivity.class);
                startActivity(intent);
            }
        });
        newsCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewsActivity.class);
                startActivity(intent);
            }
        });
        sportsCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SportsActivity.class);
                startActivity(intent);
            }
        });



        libraryCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LibraryActivity.class);
                startActivity(intent);
            }
        });
        mentorCircleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MentorActivity.class);
                startActivity(intent);
            }
        });
    }


    private void setUpRecyclerAdapter() {
        Query query = db.collection("Nit").document("Home").collection("homeCards");
        FirestoreRecyclerOptions<Nit> options = new FirestoreRecyclerOptions.Builder<Nit>()
                .setQuery(query,Nit.class)
                .build();
        adapter = new NitAdapter(options);
        RecyclerView recyclerView = v.findViewById(R.id.recycler_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}


