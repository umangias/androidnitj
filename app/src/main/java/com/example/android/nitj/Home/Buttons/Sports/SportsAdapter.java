package com.example.android.nitj.Home.Buttons.Sports;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class SportsAdapter extends FirestoreRecyclerAdapter<Sports,SportsAdapter.SportsHolder> {

    public SportsAdapter(@NonNull FirestoreRecyclerOptions<Sports> options) {
        super(options);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull SportsHolder holder, int position, @NonNull Sports model) {
        holder.sport.setText(model.getSport());
        holder.date.setText(model.getDate());
        holder.timings.setText(model.getTimings());
        holder.venue.setText(model.getVenue());
        holder.team1.setText(model.getTeam1());
        holder.team2.setText(model.getTeam2());
        Glide.with(holder.sportImage.getContext())
                .load(model.getImageUrl())
                .fitCenter()
                .into(holder.sportImage);
    }

    @NonNull
    @Override
    public SportsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_sports_matches,viewGroup,false);
        return new SportsHolder(v);
    }

    public class SportsHolder extends RecyclerView.ViewHolder {
        TextView sport;
        TextView timings;
        TextView date;
        TextView venue;
        TextView team1;
        TextView team2;
        ImageView sportImage;
        public SportsHolder(@NonNull View itemView) {
            super(itemView);

            sport = itemView.findViewById(R.id.sportsType);
            timings = itemView.findViewById(R.id.sports_timing);
            date = itemView.findViewById(R.id.sportsDate);
            venue = itemView.findViewById(R.id.sport_venue);
            team1 = itemView.findViewById(R.id.sport_team1);
            team2 = itemView.findViewById(R.id.sport_team2);
            sportImage = itemView.findViewById(R.id.sportImage);
        }
    }
}
