package com.example.android.nitj.Home.Buttons.Photo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoFragment extends Fragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference photoColRef = db.collection("Nit").document("Home")
                                                .collection("Photography");
    private PhotoDataAdapter adapter;
    private View v;


    public PhotoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_photo, container, false);

        setUpRecyclerAdapter();

        return v;
    }

    private void setUpRecyclerAdapter() {
        Query query = db.collection("Nit").document("Home")
                        .collection("Photography");
        FirestoreRecyclerOptions<Photo> options = new FirestoreRecyclerOptions.Builder<Photo>()
                .setQuery(query,Photo.class)
                .build();
        adapter = new PhotoDataAdapter(options);
        RecyclerView rv = v.findViewById(R.id.photoRecyclerList);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.notifyDataSetChanged();
        rv.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
