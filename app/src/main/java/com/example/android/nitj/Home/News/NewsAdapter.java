package com.example.android.nitj.Home.News;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class NewsAdapter extends FirestoreRecyclerAdapter<News,NewsAdapter.NewsHolder> {

    public NewsAdapter(@NonNull FirestoreRecyclerOptions<News> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull NewsHolder holder, int position, @NonNull News model) {
        holder.view.setText(model.getView());
        holder.date.setText(model.getDate());
        holder.organisedBy.setText(model.getOrganisedBy());
        holder.topic.setText(model.getTopic());
        holder.content.setText(model.getContent());
    }

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_list,viewGroup,false);
        return new NewsHolder(v);
    }

    public class NewsHolder extends RecyclerView.ViewHolder {

        TextView view;
        TextView date;
        TextView organisedBy;
        TextView topic;
        TextView content;
        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.newsView);
            date = itemView.findViewById(R.id.newsDate);
            organisedBy = itemView.findViewById(R.id.newsBy);
            topic = itemView.findViewById(R.id.newsTopic);
            content = itemView.findViewById(R.id.newsContent);

        }
    }
}
