package com.example.android.nitj.Home.Buttons.Events;

public class Event {
    private String eventDay;
    private String eventMonth;
    private String eventLocation;
    private String eventName;
    private String eventPhotoUrl;
    private String eventId;

    private Event(){}

    public Event(String mEventDay,String mEventMonth,String mEventLocation,String mEventName,String mEventPhotoUrl,String mEventId){
        this.eventDay = mEventDay;
        this.eventMonth = mEventMonth;
        this.eventLocation = mEventLocation;
        this.eventName = mEventName;
        this.eventPhotoUrl = mEventPhotoUrl;
        this.eventId = mEventId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventDay() {
        return eventDay;
    }

    public void setEventDay(String eventDay) {
        this.eventDay = eventDay;
    }

    public String getEventMonth() {
        return eventMonth;
    }

    public void setEventMonth(String eventMonth) {
        this.eventMonth = eventMonth;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventPhotoUrl() {
        return eventPhotoUrl;
    }

    public void setEventPhotoUrl(String eventPhotoUrl) {
        this.eventPhotoUrl = eventPhotoUrl;
    }
}
