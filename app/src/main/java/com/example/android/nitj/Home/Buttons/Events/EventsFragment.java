package com.example.android.nitj.Home.Buttons.Events;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.nitj.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class EventsFragment extends Fragment {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private EventAdapter adapter;
    private EventAdapter2 adapter2;
    private EventAdapter2 adapter3;

    private View v;
    public EventsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_events, container, false);
        setUpRecyclerEventFeatured();
        setUpRecyclerEventUpcoming();
        setUpRecyclerEventWorkshop();
        return v;
    }

    private void setUpRecyclerEventFeatured() {
        Query query = db.collection("Nit").document("Event").collection("FeaturedEvents");
        FirestoreRecyclerOptions<Event> options = new FirestoreRecyclerOptions.Builder<Event>()
                .setQuery(query,Event.class)
                .build();
        adapter = new EventAdapter(options);
        RecyclerView rvFeatured = v.findViewById(R.id.featuredEventRecyclerView);
        rvFeatured.setHasFixedSize(true);
        rvFeatured.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        adapter.notifyDataSetChanged();
        rvFeatured.setAdapter(adapter);
        new PagerSnapHelper().attachToRecyclerView(rvFeatured);
    }

    private void setUpRecyclerEventUpcoming() {
        Query query = db.collection("Nit").document("Event").collection("UpcomingEvents");
        FirestoreRecyclerOptions<Event> options = new FirestoreRecyclerOptions.Builder<Event>()
                .setQuery(query,Event.class)
                .build();
         adapter2 = new EventAdapter2(options);
         RecyclerView rvUpcoming = v.findViewById(R.id.upcomingRecyclerView);
         rvUpcoming.setHasFixedSize(true);
         rvUpcoming.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
         adapter2.notifyDataSetChanged();
         rvUpcoming.setAdapter(adapter2);
    }

    private void setUpRecyclerEventWorkshop() {
        Query query = db.collection("Nit").document("Event").collection("Workshops");
        FirestoreRecyclerOptions<Event> options = new FirestoreRecyclerOptions.Builder<Event>()
                .setQuery(query,Event.class)
                .build();
        adapter3 = new EventAdapter2(options);
        RecyclerView rvUpcoming = v.findViewById(R.id.workshopRecyclerView);
        rvUpcoming.setHasFixedSize(true);
        rvUpcoming.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        adapter3.notifyDataSetChanged();
        rvUpcoming.setAdapter(adapter3);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
        adapter2.startListening();
        adapter3.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
        adapter2.stopListening();
        adapter3.stopListening();
    }
}
