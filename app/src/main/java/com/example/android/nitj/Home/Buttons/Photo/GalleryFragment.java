package com.example.android.nitj.Home.Buttons.Photo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class GalleryFragment extends Fragment {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private View v;
    private GalleryAdapter adapter;

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =inflater.inflate(R.layout.fragment_gallery, container, false);
        setUpRecyclerAdapter();
        return v;
    }

    private void setUpRecyclerAdapter() {
        Query query = db.collection("Nit").document("Home")
                .collection("Photography").document("Gallery").collection("Photos");
        FirestoreRecyclerOptions<Gallery> options = new FirestoreRecyclerOptions.Builder<Gallery>()
                .setQuery(query,Gallery.class)
                .build();
        adapter = new GalleryAdapter(options);
        RecyclerView rv = v.findViewById(R.id.galleryRecyclerList);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(getContext(),2));
        adapter.notifyDataSetChanged();
        rv.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
