package com.example.android.nitj.Home.Buttons.Sports;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class TournamentAdapter extends FirestoreRecyclerAdapter<Tournament,TournamentAdapter.TournamentHolder> {

    public TournamentAdapter(@NonNull FirestoreRecyclerOptions<Tournament> options) {
        super(options);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull TournamentHolder holder, int position, @NonNull Tournament model) {
        holder.name.setText(model.getName());
        holder.desc.setText(model.getDesc());
        Glide.with(holder.image.getContext())
                .load(model.getImageUrl())
                .fitCenter()
                .into(holder.image);

    }

    @NonNull
    @Override
    public TournamentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sports_upcoming_list,viewGroup,false);
        return new TournamentHolder(v);
    }

    public class TournamentHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView desc;
        ImageView image;
        public TournamentHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tournament_name);
            desc = itemView.findViewById(R.id.tournament_desc);
            image = itemView.findViewById(R.id.tournamentImage);
        }
    }
}
