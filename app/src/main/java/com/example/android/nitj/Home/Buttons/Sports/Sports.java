package com.example.android.nitj.Home.Buttons.Sports;

public class Sports {
    private String sport;
    private String timings;
    private String date;
    private String venue;
    private String team1;
    private String team2;
    private String imageUrl;

    private Sports(){

    }

    public Sports(String sport, String timings, String date, String venue, String team1, String team2 , String imageUrl){
        this.sport = sport;
        this.date = date;
        this.timings =timings;
        this.venue = venue;
        this.team1 = team1;
        this.team2 = team2;
        this.imageUrl = imageUrl;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
