package com.example.android.nitj.Home.Buttons.Photo;

public class Gallery {
    private String mPhotoUrl;

    private Gallery(){

    }



    public Gallery(String photoUrl){
        this.mPhotoUrl = photoUrl;
    }

    public String getmPhotoUrl() {
        return mPhotoUrl;
    }

    public void setmPhotoUrl(String mPhotoUrl) {
        this.mPhotoUrl = mPhotoUrl;
    }
}
