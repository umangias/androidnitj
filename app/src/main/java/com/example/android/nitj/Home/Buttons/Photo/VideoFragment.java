package com.example.android.nitj.Home.Buttons.Photo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.nitj.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;


public class VideoFragment extends Fragment {
    View v;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private VideoAdapter adapter;
    private RecyclerView videoRv;


    public VideoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_video, container, false);

        setUpVideoRecyclerView();

        return v;
    }

    private void setUpVideoRecyclerView() {
        Query query = db.collection("Nit").document("Home").collection("Photography").document("Video")
                .collection("VideoUrl");
        FirestoreRecyclerOptions<Video> options = new FirestoreRecyclerOptions.Builder<Video>()
                .setQuery(query,Video.class)
                .build();
        adapter = new VideoAdapter(options);
        videoRv = (RecyclerView)  v.findViewById(R.id.photo_video_list);
        videoRv.setHasFixedSize(true);
        videoRv.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.notifyDataSetChanged();
        videoRv.setAdapter(adapter);

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
