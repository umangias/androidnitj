package com.example.android.nitj.Home.Buttons.Blog;

public class Blog {

    private String mTitle;
    private String mAuthor;
    private String mInfo;
    private String mPhotoUrl;

    public Blog(){

    }

    public Blog(String title, String author, String info, String photoUrl){
        this.mTitle = title;
        this.mAuthor = author;
        this.mInfo = info;
        this.mPhotoUrl = photoUrl;

    }


    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setmAuthor(String mAuthor) {
        this.mAuthor = mAuthor;
    }

    public void setmInfo(String mInfo) {
        this.mInfo = mInfo;
    }

    public void setmPhotoUrl(String mPhotoUrl){
        this.mPhotoUrl = mPhotoUrl;
    }


    public String getmTitle() {
        return mTitle;
    }

    public String getmAuthor() {
        return mAuthor;
    }

    public String getmInfo() {
        return mInfo;
    }

    public String getmPhotoUrl(){
        return mPhotoUrl;
    }
}
