package com.example.android.nitj.Home.Buttons.Books;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.android.nitj.Home.Buttons.Clubs.ClubMember;
import com.example.android.nitj.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class BooksPdfActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private BooksAdapter adapter;
    private RecyclerView booksRv;
    public static String sem_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_pdf);

        Intent intent = getIntent();
        sem_no = intent.getStringExtra("semester_no");

        setUpRecyclerView();
    }

    private void setUpRecyclerView() {

        Query query = db.collection("Nit").document("Home").collection("Books").document("Btech").collection("Semester").document(sem_no).collection("Book");
        FirestoreRecyclerOptions<Books> options = new FirestoreRecyclerOptions.Builder<Books>()
                .setQuery(query, Books.class)
                .build();

        adapter = new BooksAdapter(options);
        booksRv = findViewById(R.id.booksPdfRv);
        booksRv.setHasFixedSize(true);
        booksRv.setLayoutManager(new GridLayoutManager(this, 2));
        adapter.notifyDataSetChanged();
        booksRv.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
