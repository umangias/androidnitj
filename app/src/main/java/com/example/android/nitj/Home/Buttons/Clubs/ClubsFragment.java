package com.example.android.nitj.Home.Buttons.Clubs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.nitj.R;

public class ClubsFragment extends Fragment {
    View v;
    private CardView criminals;
    private CardView siCrew;
    private CardView rajbhasha;
    private CardView kalakaar;
    private CardView baware;
    private CardView photography;
    private CardView music;
    private CardView quest;
    private CardView bhangra;
    private CardView panache;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v =  inflater.inflate(R.layout.fragment_clubs, container, false);
        criminals = (CardView) v.findViewById(R.id.criminals);
        siCrew = (CardView) v.findViewById(R.id.si_crew);
        rajbhasha = (CardView) v.findViewById(R.id.rajbhasha);
        kalakaar = (CardView) v.findViewById(R.id.kalakaar);
        baware = (CardView) v.findViewById(R.id.baware);
        photography = (CardView) v.findViewById(R.id.photography);
        music = (CardView) v.findViewById(R.id.music);
        quest = (CardView) v.findViewById(R.id.quest);
        bhangra = (CardView) v.findViewById(R.id.bhangra);
        panache = (CardView) v.findViewById(R.id.panache);

        criminals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Criminals");
                startActivity(intent);
            }
        });
        siCrew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Sicrew");
                startActivity(intent);
            }
        });
        kalakaar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Kalakaar");
                startActivity(intent);
            }
        });
        baware.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Baware");
                startActivity(intent);
            }
        });
        photography.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Photography");

                startActivity(intent);
            }
        });
        music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Music");
                startActivity(intent);
            }
        });
        quest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Quest");
                startActivity(intent);
            }
        });
        bhangra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Bhangra");
                startActivity(intent);
            }
        });
        rajbhasha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Rajbhasha");
                startActivity(intent);
            }
        });
        panache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ClubProfileActivity.class);
                intent.putExtra("club_name","Panache");
                startActivity(intent);
            }
        });
        return v;
    }
}
