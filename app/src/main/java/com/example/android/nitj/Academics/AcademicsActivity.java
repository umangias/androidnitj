package com.example.android.nitj.Academics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.android.nitj.R;
import com.example.android.nitj.WebViewClass;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AcademicsActivity extends AppCompatActivity {

    Unbinder unbinder;
    private Toolbar toolbar;

    @BindView(R.id.cse)
    RelativeLayout cse;
    @BindView(R.id.ece) RelativeLayout ece;
    @BindView(R.id.ice) RelativeLayout ice;
    @BindView(R.id.ee) RelativeLayout ee;
    @BindView(R.id.me) RelativeLayout me;
    @BindView(R.id.it) RelativeLayout it;
    @BindView(R.id.che) RelativeLayout che;
    @BindView(R.id.ipe) RelativeLayout ipe;
    @BindView(R.id.civil) RelativeLayout civil;
    @BindView(R.id.bt) RelativeLayout bt;
    @BindView(R.id.textile) RelativeLayout textile;
    @BindView(R.id.humanities) RelativeLayout humanities;
    @BindView(R.id.chemistry) RelativeLayout chemistry;
    @BindView(R.id.physics) RelativeLayout physics;
    @BindView(R.id.maths) RelativeLayout maths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academics);
        toolbar = findViewById(R.id.academics_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Academics");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        unbinder = ButterKnife.bind(this);

        cse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Computer Science","http://www.nitj.ac.in/cse/");

            }

        });

        ice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Instrumentation and Control");
            }
        });

        ece.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Electronics & Comm.","http://www.nitj.ac.in/ece/");
            }
        });

        ee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Electrical");
            }
        });

        me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Mechanical","http://www.nitj.ac.in/mechanical/");

            }
        });

        it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Information Technology");
            }
        });

        che.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Chemical");
            }
        });

        ipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Industrial and Production","http://www.nitj.ac.in/ipe/");
            }
        });

        civil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Civil");
            }
        });

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Biotech","http://www.nitj.ac.in/biotech/");

            }
        });

        textile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Textile","http://www.nitj.ac.in/textile-technology.pdf");

            }
        });

        humanities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Humanities");
            }
        });

        chemistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Chemistry","http://www.nitj.ac.in/chemistry/");

            }
        });

        physics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Physics","http://www.nitj.ac.in/physics/");

            }
        });

        maths.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent("Mathematics");
            }
        });

    }


    public void intent(String branch){
        Intent intent = new Intent(AcademicsActivity.this,BranchActivity.class);
        intent.putExtra("branch",branch);
        startActivity(intent);
    }
    public void intent(String branch,String webUrl){
        Intent intent = new Intent(AcademicsActivity.this,BranchActivity.class);
        intent.putExtra("branch",branch);
        intent.putExtra("webUrl",webUrl);
        startActivity(intent);
    }

    public void intentWebView(String webUrl){
        Intent intent = new Intent(AcademicsActivity.this, WebViewClass.class);
        intent.putExtra("webUrl",webUrl);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
