package com.example.android.nitj.Home.Buttons.Calendar;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.Home.Buttons.Calendar.CalEventsAdapter.EventsHolder;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class CalMonthAdapter extends FirestoreRecyclerAdapter<CalendarMonth,CalMonthAdapter.CalMonthHolder> {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CalEventsAdapter eventsAdapter;

    public CalMonthAdapter(@NonNull FirestoreRecyclerOptions<CalendarMonth> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull CalMonthHolder holder, int position, @NonNull CalendarMonth model) {
        holder.month.setText(model.getMonth());
        Glide.with(holder.monthImage.getContext())
                .load(model.getImageUrl())
                .fitCenter()
                .into(holder.monthImage);

        Query query = db.collection("Nit").document("Home").collection("Calendar");
        FirestoreRecyclerOptions<CalEvents> options = new FirestoreRecyclerOptions.Builder<CalEvents>()
                .setQuery(query,CalEvents.class)
                .build();
        eventsAdapter = new CalEventsAdapter(options);
        eventsAdapter.notifyDataSetChanged();
        eventsAdapter.startListening();
        holder.eventRv.setAdapter(eventsAdapter);
    }

    @Override
    public void startListening() {
        super.startListening();
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public CalMonthHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_month_list,viewGroup,false);
        return new CalMonthHolder(v);
    }

    public class CalMonthHolder extends RecyclerView.ViewHolder {
        TextView month;
        ImageView monthImage;
        RecyclerView eventRv;

        public CalMonthHolder(@NonNull View itemView) {
            super(itemView);

            month = itemView.findViewById(R.id.calendar_month_name);
            monthImage = itemView.findViewById(R.id.calendar_month_image);
            eventRv = itemView.findViewById(R.id.calendar_events);

            eventRv.setHasFixedSize(true);
            eventRv.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        }
    }

}
