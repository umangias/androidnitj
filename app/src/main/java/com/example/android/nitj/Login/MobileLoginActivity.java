package com.example.android.nitj.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.nitj.MainActivity;
import com.example.android.nitj.R;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

public class MobileLoginActivity extends AppCompatActivity {

    private Button sendCodeButton , verifyCodeButton;
    private EditText inputPhoneNo , inputVerificationCode;
    private ProgressDialog loadingBar;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    private FirebaseAuth mAuth;
    private DatabaseReference userRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_login);

        mAuth = FirebaseAuth.getInstance();
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");

        initializeFields();

        sendCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = inputPhoneNo.getText().toString();
                if (TextUtils.isEmpty(phoneNumber)){
                    Toast.makeText(MobileLoginActivity.this, "Please enter Phone number to continue.", Toast.LENGTH_SHORT).show();
                }
                else{
                    loadingBar.setTitle("Phone Verification");
                    loadingBar.setMessage("Please wait we are authenticating your phone.");
                    loadingBar.setCanceledOnTouchOutside(false);
                    loadingBar.show();
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            "+91" + phoneNumber,
                            60, TimeUnit.SECONDS,
                            MobileLoginActivity.this,
                            mCallbacks);
                }
            }
        });

        verifyCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCodeButton.setVisibility(View.INVISIBLE);
                inputPhoneNo.setVisibility(View.INVISIBLE);

                String verificationCode = inputVerificationCode.getText().toString();
                if (TextUtils.isEmpty(verificationCode)){
                    Toast.makeText(MobileLoginActivity.this, "Please enter Verification Code.", Toast.LENGTH_SHORT).show();
                }
                else{
                    loadingBar.setTitle("Verification Code");
                    loadingBar.setMessage("Please wait we are verifying your verification Code.");
                    loadingBar.setCanceledOnTouchOutside(false);
                    loadingBar.show();

                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId,verificationCode);
                    signInWithPhoneAuthCredential(credential);
                }
            }
        });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                loadingBar.dismiss();
                Toast.makeText(MobileLoginActivity.this, "Invalid Phone Number ,Please retry", Toast.LENGTH_SHORT).show();

                sendCodeButton.setVisibility(View.VISIBLE);
                inputPhoneNo.setVisibility(View.VISIBLE);

                verifyCodeButton.setVisibility(View.INVISIBLE);
                inputVerificationCode.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                mVerificationId = verificationId;
                mResendToken = token;
                loadingBar.dismiss();

                Toast.makeText(MobileLoginActivity.this, "Code Sent,Please check and verify.", Toast.LENGTH_SHORT).show();

                sendCodeButton.setVisibility(View.INVISIBLE);
                inputPhoneNo.setVisibility(View.INVISIBLE);

                verifyCodeButton.setVisibility(View.VISIBLE);
                inputVerificationCode.setVisibility(View.VISIBLE);

            }
        };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            String currentUserId = mAuth.getCurrentUser().getUid();
                            String deviceToken = FirebaseInstanceId.getInstance().getToken();

                            userRef.child(currentUserId).child("device_token")
                                    .setValue(deviceToken)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            loadingBar.dismiss();
                                            Toast.makeText(MobileLoginActivity.this, "You have logged i Successfully", Toast.LENGTH_SHORT).show();
                                            SendUserToMainActivity();
                                        }
                                    });
                        }
                        else{
                            String message = task.getException().toString();
                            Toast.makeText(MobileLoginActivity.this, "Error : " + message, Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void initializeFields() {

        sendCodeButton = (Button) findViewById(R.id.bt_send_code);
        verifyCodeButton = (Button) findViewById(R.id.bt_verify_code);
        inputPhoneNo = (EditText) findViewById(R.id.phone_number_input);
        inputVerificationCode = (EditText) findViewById(R.id.verification_code_input);
        loadingBar = new ProgressDialog(this);
    }

    private void SendUserToMainActivity() {
        Intent intent = new Intent(MobileLoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
