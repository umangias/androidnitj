package com.example.android.nitj.Academics;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.Home.Buttons.Clubs.ClubMember;
import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import de.hdodenhof.circleimageview.CircleImageView;

public class FacultyAdapter extends FirestoreRecyclerAdapter<Member, FacultyAdapter.FacultyHolder> {

    // layut and class are used of club members .. just a new adapter to make them clickable
    // this adapter is used in club member activity.

    public FacultyAdapter(@NonNull FirestoreRecyclerOptions<Member> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull final FacultyHolder holder, int position, @NonNull final Member model) {
        holder.name.setText(model.getName());
        Glide.with(holder.image.getContext())
                .load(model.getImage())
                .fitCenter()
                .into(holder.image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(),FacultyActivity.class);
                intent.putExtra("facultyName",model.getName());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @NonNull
    @Override
    public FacultyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_faculty_rv,viewGroup,false);
        return new FacultyHolder(v);
    }

    public class FacultyHolder extends RecyclerView.ViewHolder {
        TextView name;
        CircleImageView image;
        public FacultyHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.faculty_name);
            image = (CircleImageView) itemView.findViewById(R.id.faculty_image);

        }
    }
}

