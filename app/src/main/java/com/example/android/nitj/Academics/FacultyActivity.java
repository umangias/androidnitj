package com.example.android.nitj.Academics;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.nitj.R;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;

public class FacultyActivity extends AppCompatActivity {

    private DocumentReference myRef;

    private CircleImageView facultyImage;
    private TextView researchInterest;
    private TextView profileLink;
    private TextView adminAndRes;
    private TextView profileDownload;
    private TextView facultyName;
    private TextView facultyDesignation;
    private TextView facultyQuali;
    private TextView facultyDeppt;
    private CircleImageView facultyContact;
    private CircleImageView facultyMail;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty);
        toolbar = findViewById(R.id.faculty_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Faculty");

        Intent intent = getIntent();
        String facultyNameClicked = intent.getStringExtra("facultyName");

        facultyImage = (CircleImageView) findViewById(R.id.facultyProfilePic);
        researchInterest = (TextView) findViewById(R.id.rsearch_interest_text);
        profileLink = (TextView) findViewById(R.id.profile_links_text);
        adminAndRes = (TextView) findViewById(R.id.admin_resp_text);
        profileDownload = (TextView) findViewById(R.id.profile_download_url);
        facultyName = (TextView) findViewById(R.id.facultyName);
        facultyDesignation = (TextView) findViewById(R.id.facultyDesignation);
        facultyQuali = (TextView) findViewById(R.id.facultyQualification);
        facultyDeppt = (TextView) findViewById(R.id.facultyDeptt);
        facultyContact = (CircleImageView) findViewById(R.id.facultyDialer);
        facultyMail = (CircleImageView) findViewById(R.id.facultyMail);

        myRef = FirebaseFirestore.getInstance().collection("Nit").document("Academics").collection("Faculties")
                .document(facultyNameClicked);
        myRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable final DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                researchInterest.setText((String)documentSnapshot.get("researchInt"));
                profileLink.setText((String)documentSnapshot.get("profileLink"));
                adminAndRes.setText((String)documentSnapshot.get("admin"));
                facultyName.setText((String)documentSnapshot.get("name"));
                facultyDesignation.setText((String)documentSnapshot.get("designation"));
                facultyQuali.setText((String)documentSnapshot.get("qualification"));
                facultyDeppt.setText((String)documentSnapshot.get("department"));

                Glide.with(getApplicationContext())
                        .load((documentSnapshot.get("imageUrl")))
                        .fitCenter()
                        .into(facultyImage);

                facultyContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Uri u = Uri.parse("tel:" + (String)documentSnapshot.get("contact"));
                        Intent i = new Intent(Intent.ACTION_DIAL, u);
                        startActivity(i);
                    }
                });

                facultyMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto",(String)documentSnapshot.get("email"), null));
                        startActivity(Intent.createChooser(emailIntent, "Send email..."));
                    }
                });

                profileDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(documentSnapshot.getString("profileDownload")));
                        startActivity(intent);
                    }
                });

            }
        });





    }
}

