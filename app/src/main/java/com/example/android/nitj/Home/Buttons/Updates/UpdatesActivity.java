package com.example.android.nitj.Home.Buttons.Updates;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.android.nitj.Home.Buttons.Blog.Blog;
import com.example.android.nitj.R;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class UpdatesActivity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private UpdateAdapter adapterStudent;
    private UpdateAdapter adapterNews;
    private UpdateAdapter adapterDownload;
    private UpdateAdapter adapterScholar;

    private RecyclerView recyclerUpdateStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updates);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        studentList(recyclerUpdateStudent);
        newsList(recyclerUpdateStudent);
        downloadList(recyclerUpdateStudent);
        scholarList(recyclerUpdateStudent);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void studentList(View view) {
        Query query = db.collection("Nit").document("Home")
                .collection("Update").document("UpdateButtons").collection("Student");
        FirestoreRecyclerOptions<Update> options = new FirestoreRecyclerOptions.Builder<Update>()
                .setQuery(query,Update.class)
                .build();
        adapterStudent = new UpdateAdapter(options,getApplicationContext());
        recyclerUpdateStudent = findViewById(R.id.updateRecyclerView);
        recyclerUpdateStudent.setHasFixedSize(true);
        recyclerUpdateStudent.setLayoutManager(new LinearLayoutManager(this));
        adapterStudent.notifyDataSetChanged();
        recyclerUpdateStudent.setAdapter(adapterStudent);
        adapterStudent.startListening();
    }

    public void newsList(View view) {
//        setUpRecyclerAdapter("News");
        Query query = db.collection("Nit").document("Home")
                .collection("Update").document("UpdateButtons").collection("News");
        FirestoreRecyclerOptions<Update> options = new FirestoreRecyclerOptions.Builder<Update>()
                .setQuery(query,Update.class)
                .build();
        adapterNews = new UpdateAdapter(options,getApplicationContext());
        recyclerUpdateStudent = findViewById(R.id.updateRecyclerView);
        recyclerUpdateStudent.setHasFixedSize(true);
        recyclerUpdateStudent.setLayoutManager(new LinearLayoutManager(this));
        adapterNews.notifyDataSetChanged();
        recyclerUpdateStudent.setAdapter(adapterNews);
        adapterNews.startListening();

    }

    public void downloadList(View view) {
        Query query = db.collection("Nit").document("Home")
                .collection("Update").document("UpdateButtons").collection("Download");
        FirestoreRecyclerOptions<Update> options = new FirestoreRecyclerOptions.Builder<Update>()
                .setQuery(query,Update.class)
                .build();
        adapterDownload = new UpdateAdapter(options,getApplicationContext());
        recyclerUpdateStudent = findViewById(R.id.updateRecyclerView);
        recyclerUpdateStudent.setHasFixedSize(true);
        recyclerUpdateStudent.setLayoutManager(new LinearLayoutManager(this));
        adapterDownload.notifyDataSetChanged();
        recyclerUpdateStudent.setAdapter(adapterDownload);
        adapterDownload.startListening();

    }

    public void scholarList(View view) {
        Query query = db.collection("Nit").document("Home")
                .collection("Update").document("UpdateButtons").collection("Scholarship");
        FirestoreRecyclerOptions<Update> options = new FirestoreRecyclerOptions.Builder<Update>()
                .setQuery(query,Update.class)
                .build();
        adapterScholar = new UpdateAdapter(options,getApplicationContext());
        recyclerUpdateStudent = findViewById(R.id.updateRecyclerView);
        recyclerUpdateStudent.setHasFixedSize(true);
        recyclerUpdateStudent.setLayoutManager(new LinearLayoutManager(this));
        adapterScholar.notifyDataSetChanged();
        recyclerUpdateStudent.setAdapter(adapterScholar);
        adapterScholar.startListening();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        setUpRecyclerAdapter("Student");
//        adapter.startListening();
        studentList(recyclerUpdateStudent);
        adapterStudent.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapterStudent.stopListening();
        adapterNews.stopListening();
        adapterDownload.stopListening();
        adapterScholar.stopListening();
    }

}
