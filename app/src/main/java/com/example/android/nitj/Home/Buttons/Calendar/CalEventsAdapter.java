package com.example.android.nitj.Home.Buttons.Calendar;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.nitj.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class CalEventsAdapter extends FirestoreRecyclerAdapter<CalEvents,CalEventsAdapter.EventsHolder> {

    public CalEventsAdapter(@NonNull FirestoreRecyclerOptions<CalEvents> options) {
        super(options);
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @Override
    protected void onBindViewHolder(@NonNull EventsHolder holder, int position, @NonNull CalEvents model) {
        holder.day.setText(model.getDay());
        holder.date.setText(model.getDate());
        holder.event.setText(model.getEvent());
        holder.desc.setText(model.getDesc());
    }

    @Override
    public void startListening() {
        super.startListening();
    }

    @NonNull
    @Override
    public EventsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.calendar_events_list,viewGroup,false);
        return new EventsHolder(v);
    }

    public class EventsHolder extends RecyclerView.ViewHolder {
        TextView date;
        TextView day;
        TextView event;
        TextView desc;
        public EventsHolder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.calendar_event_date);
            day = itemView.findViewById(R.id.calendar_event_day);
            event = itemView.findViewById(R.id.calendar_event_name);
            desc = itemView.findViewById(R.id.calendar_event_desc);
        }
    }
}
